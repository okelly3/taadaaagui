##	Instructions on how to deploy to App Engine

** Get the sources and build		* *

		* git clone https://okelly3@bitbucket.org/okelly3/taadaaagui.git
		* cd taadaaagui
		* npm install
		* optional: configure api endpoint in src/assets/envconfig.js file.
		* ng build --prod

** Prepare GCP		* *

		* In GCP create a new project and under "Cloud Storage\Browser" create a new Bucket called "taadaaagui"
		* upload the dist folder created by ng build to the new bucket
		* upload app.yaml and cloudbuild.yaml from the root folder to the new bucket
		* you should noe have 2 files and a folder in the bucket root

		* in could shell select the project you craeted
		* create a new folder called gui:
								mkdir gui
								cd gui
		* copy the files from the bucket to the new folder:
								gsutil rsync -r gs://taadaaagui .
		* create new app:
				gcloud app create
		* select region

		* deploy app:
			gcloud app deploy
		* browse app:
			gcloud app browse

** Running Sample **
	available here:	https://vcita-gui.ue.r.appspot.com/