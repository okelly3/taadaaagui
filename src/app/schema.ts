export class Task {
    id: string = '';
    title: string = '';
    is_complete: boolean = false;
}