import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Task } from './schema';


@Injectable({
    providedIn: 'root',
})

export class ApiService {

    constructor(@Inject(forwardRef(() => HttpClient)) private http: HttpClient) { }

    private errorHandler(error: HttpErrorResponse) {
        return throwError((error.error.status) ? `${error.error.status} - ${error.error.detail}` : `${error.message}`);
    }

    post_task(title: string): Observable<any> {
        let newTask :Task = new Task;
        newTask.title = title
        return this.http.post(`${environment.webapiurl}task`, newTask).pipe(
            catchError(this.errorHandler)
        );
    }
    
    delete_task(id: string): Observable<any> {
        return this.http.delete(`${environment.webapiurl}task/${id}`).pipe(
            catchError(this.errorHandler)
        );
    }

    put_task(id: string): Observable<any> {
        return this.http.put(`${environment.webapiurl}task/${id}`, null).pipe(
            catchError(this.errorHandler)
        );
    }

    get_list(): Observable<any> {
        console.log(`${environment.webapiurl}list/all`);
        return this.http.get(`${environment.webapiurl}list/all`).pipe(
            catchError(this.errorHandler)
        );
    }
}