import { ValueProvider } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from './apiservice'
import { Task } from './schema';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'TaaDaaa';
  webapiurl = environment.webapiurl;
  list: Task[];
  displayAddDialog: boolean = false;
  addDialog_title: string = '';
  errMsg: string = '';

  constructor(private api: ApiService) {

  }

  ngOnInit(): void {
    this.get_list();
  }

  button(target): void {
    if (target.name == 'save') {
      this.api.post_task(this.addDialog_title).subscribe(
        {
          next: newTask => this.list.push(newTask),
          error: err => this.errMsg = err
        }
      );
    }
    this.addDialog_title = '';
    this.displayAddDialog = false;
  }

  delete_task(task: Task): void {
    this.api.delete_task(task.id).subscribe(
      {
        next: t => this.list.splice(this.list.findIndex(t => t.id == task.id), 1),
        error: err => this.errMsg = err
      }
    );
  }

  put_task(task: Task): void {
    this.api.put_task(task.id).subscribe(
      {
        next: t => task.is_complete = !task.is_complete,
        error: err => this.errMsg = err
      }
    );
  }

  get_list(): void {
    this.api.get_list().subscribe(
      {
        next: list => this.list = list,
        error: err => this.errMsg = err
      }
    );
  }
}
